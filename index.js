exports.handler = async (event) => {
    
    let receivedString = event.inputstring;
    const outputString = receivedString.replace(/(?<!\w)(Google|Oracle|Amazon|Deloitte|Microsoft)(?=[\s.?,!]|$)/g, function(matched){
            return matched + "©";
            });
            
    const response = { statusCode: 200, outputString: outputString };
    return response;
    
};