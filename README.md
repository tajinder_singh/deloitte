# Deloitte - Assignment
This project contains Node js code for AWS lambda function which when integrated correctly with AWS apigateway exposes an api that recieves a string as input
replaces certain words depending on few conditions and returns modified string.

## Goal
Build an API that will use a string as input and does a find and replace for certain words and outputs the result. For example: replace Google for Google©.
The words that need to be replaced are provided below:
* Oracle -&gt; Oracle©
* Google -&gt; Google©
* Microsoft -&gt; Microsoft©
* Amazon -&gt; Amazon©
* Deloitte -&gt; Deloitte©


## How to consume this API

Please add the inoput string at the end of the API URL. The GET request takes in string with parameter as "rawString".

https://hhzp4j81mf.execute-api.eu-central-1.amazonaws.com/deloitte-exam/inputstring?rawString=PLACE_YOUR_STRING_HERE

## Project Overview
This read me file contains following seciton which will help you better understand and consume this api :
* Api URL 
    * query parameters for get request
    * response structure
* Architecture Overview
* AWS Lambda Overview
* Api Gateway Overview


## Api URL

Below is the URL for the api that fulfills the goal of this assignment.

                    https://hhzp4j81mf.execute-api.eu-central-1.amazonaws.com/deloitte-exam/inputstring?rawString=PLEASE PUT A STRING OF YOUR CHOICE
                    
* query parameters for get request

In order to send a string , use a query parameters as "rawString". Please use this parameter to send the string that you want to transform accoridng to the reuirement of this section
            Query Parameter
            Name     = rawString
            required = no
            If not provided = empty string will be treated as value for rawString parameter.

* response structure

Once the get requested is succesfull, you get the following JSON response structure back from the API

                { 
                    statusCode: 200,
                    outputString: THE NEW STRING AFTER REQUIRED WORDS WERE REPLACED
                }
    
## Architecture overview
![](./gitlab.jpg)

* Step 1 : User sends a GET request with string that needs to be replaced.
* Step 2 : AWS Apigateway recieves the call, and process the request, associate it with the correct Api resource group.It mapps the query paramters string to JSON object thats passed to lambda.
* Step 3 ; AWS apigateway triggers the lambda function that is integrated with Api endpoint.
* Step 4 : Lambda function which is a Node Js program, recieve the event with string , replaces all strings that needs to be replaced and creates a response body.
* Step 5 : Lambda function sends back the response body containing status code and new output string to Apigateway.
* Step 6 : Api gateway recives the response from lambda and sends it back to the user that intitated the get request.
* Step 7 : User gets the response body containing the output string.


## AWS Lambda Overview

AWS lambda function is written in Node js language. The index.js file in this repository is the actual code of the lambda function.
The lambda function is triggered when Api gateway on AWS cloud recieves the GET request. Api gateway passes the input string parameters in a JSON format to the
lambda function. When the function is invoked, this json is passed and using regex ,we replace the set of words with © in the end.

If you need more informaiton about AWS lambda, please check official AWS documentation on this [link](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)

## Api Gateway Overview 

Amazon API Gateway is a fully managed service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale.
The gateway created for this task, has a resource group with name as input string with mapped query parameters as application/json format. The api is deployed on stage
called as deloitte-exam.

If you need more information about AWS Gateway, please check official AWS documentation on this [link](https://aws.amazon.com/api-gateway/)